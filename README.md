# Default Static Gulp

This is a Gulp-4.00 starter kit for static projects.

## Setup

1. Install dependencies

```
$ yarn install
// OR
$ npm install
```

## Commands

`watch` - **gulp watch** task. Runs the localserver on port 3000 and
watches all file changes in /css/, /js/, and /img/ folders and subfolders;

`build` - **gulp default** task. Builds the project assets.

`build:css` - **gulp css** task. Outputs the minified version of bundled styles.

`build:js` - **gulp js** task. Outputs the minified version of bundled scripts.

`build:img` - **gulp img** task. Outputs the optimized version of project images.

## SCSS folder structure

```
/scss/
  /01-variables/
    Project global variables, font-definitions
  /02-functions/
    SASS functions
  /03-mixins/
    SASS mixins
  /04-typography/
    Default typography styling
  /05-global/
    Global elements styling
  /06-components/
    Component styling
```
